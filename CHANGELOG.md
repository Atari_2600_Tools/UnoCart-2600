# Changelog

## v18

 * Fix Survival Island and probably other supercharger games.

## v17

 * Improve startup time.

## v16

 * Suppoer 3E+ with >16kB RAM.

## v15

 * 4kSC support.

## v14

 * Support for 3E+ (thanks to Wolfgang Stubig / AlNafuur on AAge)

## v13

 * Improve DPC compatibility.

## v12

 * Fix DPC regressions on some machines
 * Improve DPC audio quality

## v11

 * Increase optimization level to O2 -> supercharger games work again.

## v10

 * Revert some of the autodetection changes; should fix F8 carts that fail to detect.

## v09

 * Support for the Pink Panther Prototype
 * Support for BF / BFSC
 * Support for DF / DFSC
